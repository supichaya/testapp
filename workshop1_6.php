<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
    .makeBigBlue{
        color:blue;
        font-size:28px;
    }
    </style>
    <title>Document</title>
</head>
<body>
<form action="workshop1_view_json.php" method="POST">
        First name : <input type="text" id="firstname" name="firstname" placeholder="กรุณาใส่ชื่อ">
        Last name : <input type="text" id="lastname" name="lastname" placeholder="กรุณาใส่นามสกุล">
        <button type="submit">View</button>
        <input type="text" id="result">
        <input type="text" id="result2">
        <button type="button" id="enable">Enabled</button>
        <button type="button" id="clear">Clear</button>
</form>
<div></div>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  <script>
  $(function () {
      $('#enable').click(function (e) { //jqclick
          e.preventDefault();
          $('#firstname').removeAttr('disabled');//jqattrremove
          $('#lastname').removeAttr('disabled');
          
      });
      $('#clear').click(function (e) { 
          e.preventDefault();

/*
          $('#firstname').val("");//jqval
          $('#lastname').val("");
          $('#result').val("");
          $('#result2').val("");
          $('#firstname').focus();
          */

          $('input').val("");
          $('#firstname').focus();
          $('#result').removeClass("makeBigBlue");
          
      });
      $('form').submit(function (e) { 
          e.preventDefault();
          if (!validation()){
              return false;
          }

          alert($(this).serialize());
          $.ajax({
              type: "GET",
              url: $(this).attr('action'),
              data: $(this).serialize(),
              dataType: "JSON",
              success: function (response) {
                  //alert(response.firstname);
                  $('#result').val(response.firstname);
                  $('#result2').val(response.lastname);

$('#result').addClass("makeBigBlue");
                  $('#firstname').attr('disabled', true);//key jqattrset
                  $('#lastname').attr('disabled', true);
              }
              
          });
  });
  });
function validation(){
    var v_msg="";

var v_firstname=$('#firstname');
var v_lastname=$('#lastname');

if ($.trim(v_firstname.val())=="") {
    v_msg=v_msg+'firstname'
}
    if ($.trim(v_lastname.val())=="") {
    v_msg=v_msg+'lastname'
    }
    if (v_msg.length>0) {
    alert("please input"+v_msg);
    return false;
    }

    return true;
}
    
  </script>
</body>
</html>