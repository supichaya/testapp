<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>FORM</title>
  </head>
  <body>
  
  <form>
  
  <table border="0" width="62%" align="center">
  <tr>
  <div class="form-row">
    <div class="col">
    <th width="10%" align="right">ชื่อ :</th>
    <td width="20%" align="left"><input type="text" id="firstname" name="firstname" class="form-control" placeholder="กรุณาใส่ชื่อ" maxlength=50></td>
    </div>
    <td width="2%" align="left"></td>
    <div class="col">
    <th width="10%" align="right">นามสกุล :</th>
    <td width="20%" align="left"><input type="text" id="lastname" name="lastname" class="form-control" placeholder="กรุณาใส่นามสกุล" maxlength=50></td>
    </div>
  </div>
  </tr>
  </table>
  <table border="0" width="62%" align="center">
  <tr>
  <div class="form-group col-md-4">
  
  <th width="10%" align="right">ตำแหน่ง : <label for="inputState"></label></th>
      <td width="20%" align="left"><select id="inputState" class="form-control">
        <option selected>กรุณาเลือก</option>
        <option>Developer</option>
        <option>SA</option>
        <option>PM</option>
      </select></td>
      </div>
      <td width="2%" align="left"></td>
      <th width="10%" align="right"></th>
      <td width="20%" align="left"></td>
      
      </tr>
      </table>
      <table border="0" width="62%" align="center">
      <th width="10%" align="right"></th>
      <td width="20%" align="right"><button type="clear" id="clear" class="btn btn-primary">Clear</button></td>
      <td width="2%" align="left"></td>
      <th width="10%" align="right"><button type="save" id="submit"class="btn btn-primary">Save</button></th>
      <td width="20%" align="left"></td>
      </table>
</form>
<script>

</script>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

<script>
$(function () {
      $('form').submit(function (e) { 
          //e.preventDefault();
          var v_msg="";

          var v_firstname=$('#firstname');
          var v_lastname=$('#lastname');
        
          if ($.trim(v_firstname.val())=="") {
              v_msg=v_msg+'firstname'
          }
              if ($.trim(v_lastname.val())=="") {
              v_msg=v_msg+'lastname'
              }
              if (v_msg.length>0) {
              alert(v_msg);
              return false;

              $('#firstname').attr('disabled', true);//key jqattrset
              $('#lastname').attr('disabled', true);
              }

              return true;
              
      });
  });
  
</script>
</body>

</html>