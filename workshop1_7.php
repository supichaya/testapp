<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
    .makeBigBlue{
        color:blue;
        font-size:28px;
    }
    </style>
    <title>Document</title>
</head>
<body>
<form action="workshop1_view_json.php" method="POST">
        First name : <input type="text" id="firstname" name="firstname" placeholder="กรุณาใส่ชื่อ" data-title="first name">
        Last name : <input type="text" id="lastname" name="lastname" placeholder="กรุณาใส่นามสกุล" data-title="last name">
        <button type="submit">View</button>
        
        <button type="button" id="enable">Enabled</button>
        <button type="button" id="clear">Clear</button>
</form>

<input type="text" id="result">
<input type="text" id="result2">
<div></div>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  <script>
  $(function () {
      $('#enable').click(function (e) { //jqclick
          e.preventDefault();
          $('#firstname').removeAttr('disabled');//jqattrremove
          $('#lastname').removeAttr('disabled');
          
      });
      $('#clear').click(function (e) { 
          e.preventDefault();

/*
          $('#firstname').val("");//jqval
          $('#lastname').val("");
          $('#result').val("");
          $('#result2').val("");
          $('#firstname').focus();
          */

          $('input').val("");
          $('#firstname').focus();
          $('#result').removeClass("makeBigBlue");
          
      });
      $('form').submit(function (e) { 
          e.preventDefault();
          if (!validation()){
              return false;
          }

          alert($(this).serialize());
          $.ajax({
              type: "GET",
              url: $(this).attr('action'),
              data: $(this).serialize(),
              dataType: "JSON",
              success: function (response) {
                  //alert(response.firstname);
                  $('#result').val(response.firstname);
                  $('#result2').val(response.lastname);

$('#result').addClass("makeBigBlue");//jqaddclass
                  $('#firstname').attr('disabled', true);//key jqattrset
                  $('#lastname').attr('disabled', true);
              }
              
          });
  });
  });
  
function validation() {
   
    $('form input').each(function (index, element) {
        //jqeachelement
        var v_obj = $(this);
        var v_msg = $(this).attr('data-title');

        if (v_obj.val()=="") {
            alert('Please input' +v_msg)
            v_obj.focus();
            return false;
        }
        
    });
    return true;
}
    
  </script>
</body>
</html>